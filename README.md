# Redmine Issues by Date Grouping plugin

Allows grouping issues by Start date, Due date, Created and Closed in Redmine at `/issues` in `Group results by` selection field.

## Installation

Just clone this repository to `./plugins` directory of your Redmine installation and restart unicorn.

## Credits

Plugin was based on the patch https://www.redmine.org/attachments/download/15226/issue_query.rb.patch that was posted at https://www.redmine.org/issues/13803.

## License

This Redmine plugin contains Ruby backend-only code and is distributed under GPLv2.
