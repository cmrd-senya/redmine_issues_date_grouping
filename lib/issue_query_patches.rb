module IssueDateGrouping
  module Patches
    module IssueQueryPatch
      def self.included(base) # :nodoc:
        base.available_columns.select {|column|
          %i[start_date due_date created_on closed_on].include?(column.name)
        }.each {|column|
          column.groupable = column.name.to_s
        }
      end
    end
  end
end

IssueQuery.send(:include, IssueDateGrouping::Patches::IssueQueryPatch)
