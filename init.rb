require 'redmine'

Redmine::Plugin.register :issue_date_grouping do
  name 'Issues date grouping'
  author 'Senya'
  description 'Allows grouping issues by Start date, Due date, Created and Closed'
  version '0.1.0'
  url 'https://gitlab.com/cmrd-senya/redmine_issues_date_grouping'
end

require_relative 'lib/issue_query_patches'
